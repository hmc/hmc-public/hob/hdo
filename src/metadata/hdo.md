---
layout: ontology_detail
id: hdo
title: Helmholtz Digitisation Ontology
jobs:
  - id: https://travis-ci.org/cct7/hdo
    type: travis-ci
build:
  checkout: git clone https://github.com/cct7/hdo.git
  system: git
  path: "."
contact:
  email: 
  label: 
  github: 
description: Helmholtz Digitisation Ontology is an ontology...
domain: stuff
homepage: https://github.com/cct7/hdo
products:
  - id: hdo.owl
    name: "Helmholtz Digitisation Ontology main release in OWL format"
  - id: hdo.obo
    name: "Helmholtz Digitisation Ontology additional release in OBO format"
  - id: hdo.json
    name: "Helmholtz Digitisation Ontology additional release in OBOJSon format"
  - id: hdo/hdo-base.owl
    name: "Helmholtz Digitisation Ontology main release in OWL format"
  - id: hdo/hdo-base.obo
    name: "Helmholtz Digitisation Ontology additional release in OBO format"
  - id: hdo/hdo-base.json
    name: "Helmholtz Digitisation Ontology additional release in OBOJSon format"
dependencies:
- id: bfo
- id: iao
- id: obi
- id: sdgio
- id: ro
- id: pato

tracker: https://github.com/cct7/hdo/issues
license:
  url: http://creativecommons.org/licenses/by/3.0/
  label: CC-BY
activity_status: active
---

Enter a detailed description of your ontology here. You can use arbitrary markdown and HTML.
You can also embed images too.

