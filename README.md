
# Helmholtz Digitisation Ontology (HDO)

<img src="HDO_logo.png" alt="HDO_logo" width="250" >

This Helmholtz Digitization Ontology (HDO) is a mid-level ontology that contains concepts representing digital assets relevant to the Helmholtz digital ecosystem, data creation, management, and exchange.
The scope of the HDO is, to provide precise and comprehensive semantics of the concepts and practices used to manage digital assets within the [Helmholtz association of research centers](https://www.helmholtz.de/en/) that are harmonized across all Helmholtz [research fields](https://www.helmholtz.de/en/research/research-fields/). 
HDO is developed within the framework of the [Helmholtz Metadata Collaboration (HMC)](https://helmholtz-metadaten.de/en) with contributors from various scientific backgrounds.
It is released as a machine-readable artifact with high semantic expressiveness and top-level alignment to the [BFO](https://basic-formal-ontology.org/).


### Files

<table>
    <thead>
        <tr>
            <th>Version</th>
            <th>File name</th>
            <th>IRI</th>
            <th>Comment</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td rowspan=4 valign="bottom">[Release Artefacts](https://oboacademy.github.io/obook/reference/release-artefacts/)</td>
            <td>hdo.owl</td>
            <td>[https://purls.helmholtz-metadaten.de/hob/hdo/hdo.owl](https://purls.helmholtz-metadaten.de/hob/hdo/hdo.owl)</td>
            <td>latest stable version</td>
        </tr>
        <tr>
            <td>hdo-base.owl</td>
            <td>[https://purls.helmholtz-metadaten.de/hob/hdo/hdo-base.owl](https://purls.helmholtz-metadaten.de/hob/hdo/hdo-base.owl)</td>
            <td>A version of the ontology that does not include any externally imported axioms</td>
        </tr>
        <tr>
            <td>hdo-full.owl</td>
            <td>[https://purls.helmholtz-metadaten.de/hob/hdo/hdo-full.owl](https://purls.helmholtz-metadaten.de/hob/hdo/hdo-full.owl)</td>
            <td>A version of the ontology that contains all logical axioms, including inferred subsumptions</td>
        </tr>
        <tr>
        </tr>
        <tr>
            <td>Development</td>
            <td>hdo-edit.owl</td>
            <td>[.../ontology/hdo-edit.owl](src/ontology/hdo-edit.owl)</td>
            <td>editors version</td>
        </tr>
</table>

The development version HDO (hdo-edit.owl) imports all ontologies/files needed for the full version of the HDO ontology:
 - [Basic Formal Ontology](http://purl.obolibrary.org/obo/bfo.owl) (latest release)
 - [Information Artifact Ontology](http://purl.obolibrary.org/obo/iao.owl) (latest release)
 - [Relation Ontology Core subset](http://purl.obolibrary.org/obo/ro/core.owl) (latest release)
 
## Contributing
Thank you for being interested in HDO. We are looking forward to getting in touch with you regarding any questions or inquiries you might have regarding HDO. You may contact us via hmc(at)fz-juelich.de or via helpdesk(at)helmholtz-metadaten.de.
To contribute directly—e.g., requesting new terms & classes, reporting errors, or raising specific concerns related to the onology—you may also use the HDO [issue tracker](https://codebase.helmholtz.cloud/hmc/hmc-public/hob/hdo/-/issues).

## Contributions
We kindly thank all authors and contributors who are mentioned in the ontology metadata and in records regarding microcontributions of specific classes!

## Further reading 
For further information regarding related activities and developments, please see: 
- **Persistent identifiers for semantic artifacts (PIDA):** Hofmann, V. (2024, April 27). PIDA - persistent identifiers for digital assets: powering semantic artifacts with PURLs. Zenodo. https://doi.org/10.5281/zenodo.11077247
- **Helmholtz Metadata Collaboration - webpage:** https://helmholtz-metadaten.de/en 
- **Helmholtz Metadata Collaboration—news on LinkedIn:** https://www.linkedin.com/company/100846014/

## Documentation
For further human-readable documentation concerning HDO terms, see the [HTML Documentation Page](https://materials-data-science-and-informatics.github.io/Helmholtz-Digitisation-Ontology-Documentation/index.html#https://purls.helmholtz-metadaten.de/hob/HDO_00000000).

## License
These documents are licensed under the terms of the Creative Commons Attribution 4.0 International (CC BY 4.0) license.

## Acknowledgements

<div>
<img style="vertical-align: middle;" alt="ODK Logo" src="https://github.com/jmcmurry/closed-illustrations/raw/master/logos/odk-logos/odk-logo_black-banner.png" height=80/>
&nbsp;&nbsp;
<img style="vertical-align: middle;" alt="OBO Logo" src="https://obofoundry.org/images/foundrylogo.png" height=80/>
</div>
<br />
This ontology repository was created using the [Ontology Development Kit (ODK)](https://github.com/INCATools/ontology-development-kit). Development principles and practices are inspired by [OBO Foundry](https://obofoundry.org/). 

## Funding

<div>
<img style="vertical-align: middle;" alt="HMC Logo" src="https://github.com/Materials-Data-Science-and-Informatics/Logos/raw/main/HMC/HMC_Logo_M.png" height=65/>
</div>
<br />
This project is a collaborative development and funded by the [Helmholtz Metadata Collaboration (HMC)](https://helmholtz-metadaten.de/en), an incubator platform of the Helmholtz Association within the framework of the Information and Data Science strategic initiative.
