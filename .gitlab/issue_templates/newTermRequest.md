To request a new term, pleae fill out the following sections. In each section, you can add content in multiple languages, but please always have an English entry available.

### Term labels

Please include the label or labels you would like your term to have. For example, "sloth" and "faultier". 

### Term definition

Please include a concise and minimal definition of your term. For example, "A group of arboreal Neotropical xenarthran mammals, constituting the suborder Folivora" and "Die Faultiere (Folivora, auch Tardigrada oder Phyllophaga) bilden eine Unterordnung der zahnarmen Säugetiere (Pilosa)"

### Definition sources

For the examples above, these would be https://en.wikipedia.org/wiki/Sloth and https://de.wikipedia.org/wiki/Faultiere 

### Examples

Please include real-world examples of the term you're proposing, including links to sources. For example "Mira"https://slothconservation.org/the-true-stories-behind-these-famous-baby-sloth-photos/ 

### Term synonyms 

If your term has any synonyms, please enter them in the bulleted list below. Exact synonyms are entirely interchangeable: the instances of one term match the instances of the other. Broad synonyms are terms that include all instances of your term, but also other entities. Narrow synonyms are terms that include only a subset of your term's instances. Related synonyms are semantically close, but with fuzzy or complex relationships.

 * Exact: 
 * Broad: "Folivora"
 * Narrow: "three-toed sloths", "dreifinger faultier"
 * Related: "Megatherium"

### Term acronyms

If your term has any acronyms, please enter them here.

### Term abbreviations

If your term has any non-acronym abbreviations, please enter them here.

### Comments

Here, you can add additional comments to supplement the definition. For example, typical properties or characteristics of the entity you're describing. Please also include sources if any.

For the examples above, one could add: 
 * "Noted for their slowness of movement, they spend most of their lives hanging upside down in the trees of the tropical rainforests of South America and Central America." https://en.wikipedia.org/wiki/Sloth
 * "Bekannt sind die Faultiere vor allem durch ihre – mit dem Rücken nach unten – im Geäst hängende Lebensweise, ihre sehr langsamen Bewegungen und die langen Ruhephasen." https://de.wikipedia.org/wiki/Faultiere 

### Nano-crediting

Please enter the ORCIDs or similar unique and permanent identifiers of all contributors to this request. If you don't have an ORCID, you can aquire one here: https://orcid.org/
