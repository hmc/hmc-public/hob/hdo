You are or want to use HDO in your use case or aling to it? You are missing classes or relations? 

Please get in contact with us - we are really looking forward to feedback and adapting HDO as a Helmholtz-wide frame for reference that can semantically connect across domains. The easiest ist if you write your request into our [issue board](https://codebase.helmholtz.cloud/hmc/hmc-public/hob/hdo/-/boards). WHen doing so please follow these steps: 

## :eyes: Before you write a new request, please consider the following: 
- **Does the term already exist?** Before submitting suggestions for new ontology terms, check whether the term exist, either as a primary term or a synonym term. You can search using [OLS](http://www.ebi.ac.uk/ols/ontologies/hdo)

## :construction: Guidelines for creating GitLab tickets with contributions to the ontology:
1. **Write a detailed request:** Please be specific and include as many details as necessary, providing background information, and if possible, suggesting a solution. GOC editors will be better equipped to address your suggestions if you offer details regarding *'what is wrong'*, *'why'*, and *'how to fix it'*.
2. **Provide examples and references:** Please include PMIDs for new term requests, and include also screenshots, or URLs illustrating the current ontology structure for other types of requests. 
3. **For new term request:** Be sure to provide suggestions for label (name), definition, position in hierarchy, references, comments, etc.
4. **For updates to relationships:** Provide details of the current axioms, why you think they are wrong or not sufficient, and what exactly should be added or removed.

On behalf of the Helmholtz Digitisation Ontology editorial team: Thank you for your interest in HDO - we are looking forward to your input in contribution